(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("countries", {
                url: "/countries",
                templateUrl: "/views/countries.html",
                controller: "SelectCtrl",
                controllerAs: "ctrl"
            })
            .state("currency", {
                url: "/currency/:code/:i",
                templateUrl: "/views/currency.html",
                controller: "CurrencyCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise("/countries");
    }


})();