(function () {
    'use strict';

    angular
        .module('MyApp')
        .controller('CurrencyCtrl', CurrencyCtrl);

    CurrencyCtrl.$inject = ["$state", "$stateParams", "MyAppService", "$ionicLoading"];
    function CurrencyCtrl($state, $stateParams, MyAppService, $ionicLoading) {
        var vm = this;
        vm.data = null;
        vm.showNext = showNext;
        vm.index = 0;

        init();

        //////////////////////

        function showNext() {
            var len = vm.data.rates.length;
            var i = (vm.index + 1) % len;
            // console.log(i);
            $state.go("currency", { code: vm.data.rates[i].currency, i: i })
        }

        function init() {
            const base = $stateParams.code;
            vm.index = parseInt($stateParams.i);
            console.log(vm.index);
            // console.log(code);

            $ionicLoading.show({
                template: "Retrieving rates..."
            });

            MyAppService.getRates(base).then(function (result) {
                console.log(result);
                vm.data = result;
                $ionicLoading.hide();
            }).catch(function (err) {

            });

        }
    }
})();