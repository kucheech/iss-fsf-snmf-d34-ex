(function () {
    'use strict';

    angular
        .module('MyApp')
        .controller('SelectCtrl', SelectCtrl);

    SelectCtrl.$inject = ["$state", "MyAppService", "$ionicLoading", "$scope"];
    function SelectCtrl($state, MyAppService, $ionicLoading, $scope) {
        var vm = this;
        vm.countries = [];
        vm.showCurrency = showCurrency;
        vm.refresh = refresh;

        init();

        /////////////////////

        function refresh() {
            MyAppService.getCountries().then(function (result) {
                console.log(result);
                // vm.countries = result.countries;
                $scope.$broadcast("scroll.refreshComplete");
            }).catch(function (err) {
                console.log(err);
            });
        }

        function showCurrency(code, i) {
            // console.log(i);
            $state.go("currency", { code: code, i: i });
        }

        function init() {
            $ionicLoading.show({
                template: "Retrieving countries list..."
            });
            // console.log("init");
            MyAppService.getCountries().then(function (result) {
                // console.log(result);
                vm.countries = result.countries;
                $ionicLoading.hide();
            }).catch(function (err) {
                console.log(err);
            });

        }
    }
})();