(function () {
    'use strict';

    angular
        .module('MyApp')
        .service('MyAppService', MyAppService);

    MyAppService.$inject = ['$http', "$q"];
    function MyAppService($http, $q) {
        var vm = this;
        vm.data = null;
        vm.getCountries = getCountries;
        vm.getRates = getRates;

        ////////////////

        function getCountries() {
            var defer = $q.defer();

            if (vm.data == null) {

                $http.get("/countries").then(function (result) {
                    // console.log(result);
                    vm.data = result.data;
                    defer.resolve(result.data);
                }).catch(function (err) {
                    console.log(err);
                    defer.reject(err);
                });

            } else {
                defer.resolve(vm.data);
            }

            return defer.promise;
        }


        function getRates(base) {
            // console.log("base: " + base);

            var defer = $q.defer();

            $http.get("/currency/" + base).then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }
    }

})();