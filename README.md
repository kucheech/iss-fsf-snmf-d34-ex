### to use request-promise at server/app.js
```
npm install --save request
npm install --save request-promise
```

### Ionic v1 components
http://ionicframework.com/docs/v1/components


### fixer.io

```
http://api.fixer.io/latest
{
    "base": "EUR",
    "date": "2017-10-11",
    "rates": {
        "AUD": 1.5186,
        "BGN": 1.9558,
        "BRL": 3.7515,
        "CAD": 1.4798,
        "CHF": 1.1524,
        "CNY": 7.7989,
        "CZK": 25.879,
        "DKK": 7.443,
        "GBP": 0.8971,
        "HKD": 9.2344,
        "HRK": 7.5115,
        "HUF": 309.93,
        "IDR": 15998,
        "ILS": 4.1364,
        "INR": 77.066,
        "JPY": 132.72,
        "KRW": 1339.1,
        "MXN": 22.217,
        "MYR": 4.9911,
        "NOK": 9.379,
        "NZD": 1.6723,
        "PHP": 60.863,
        "PLN": 4.2851,
        "RON": 4.5865,
        "RUB": 68.449,
        "SEK": 9.5313,
        "SGD": 1.6044,
        "THB": 39.258,
        "TRY": 4.3337,
        "USD": 1.183,
        "ZAR": 16.052
    }
}
```

### country flag icons
Icon made by Freepik from www.flaticon.com 