require('dotenv').load();

const path = require("path");
const express = require("express");
const app = express();
const rp = require('request-promise');

const countries = require("./countries");
// console.log(countries);

const NODE_PORT = process.env.port || 3000;

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));

const IMG_FOLDER = path.join(__dirname, "/img/");
app.use("/img", express.static(IMG_FOLDER));


app.get("/countries", function (req, res) {
    console.log("get /countries");
    res.status(200).send(countries);
});

app.get("/currency/:code", function (req, res) {

    const code = req.params.code;
    var options = {
        uri: "http://api.fixer.io/latest?base=" + code,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function (result) {
            //  console.log(result);
            const rates = result.rates;
            //  console.log(rates);
            var data = countries.countries;
            // console.log(data);
  
            //    console.log(countries);
            for (var i in data) {
                var c = data[i];
                //    console.log(c);
                // c.push({
                //     rate: rates[c.currency] || 1
                // });
                c["rate"] = rates[c.currency] || 1;
                // console.log(c);
            }

            result.rates = data;
            // console.log(data);
            // res.status(200).send(data);
            res.status(200).send(result);
        })
        .catch(function (err) {
            // API call failed...
        });

});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app