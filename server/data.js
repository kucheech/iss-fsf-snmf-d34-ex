const rp = require('request-promise');

const currencies = require("./currency");

var countries = [];
var options = {
    uri: "http://api.fixer.io/latest",
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
};

rp(options)
    .then(function (rates) {
        var codes = Object.keys(rates.rates);
        for(var i in codes) {
            var c = codes[i];
            var country = {
                name: currencies[c].description,
                currency: c
            };
            countries.push(country);
        }

        console.log(countries);

    })
    .catch(function (err) {
        // API call failed...
    });



